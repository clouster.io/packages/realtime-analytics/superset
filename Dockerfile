FROM apache/superset:1.5.1

RUN pip install \
        psycopg2-binary==2.9.1 \
        "pinotdb>=0.3.9" \
        redis==3.5.3