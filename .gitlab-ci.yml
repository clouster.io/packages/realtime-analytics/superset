variables:
  GIT_SUBMODULE_STRATEGY: recursive
  CSERPH_VERSION: latest
  PORTER_RUNTIME_VERSION: latest
  DOCKER_VERSION: latest
  DIND_VERSION: dind

stages:
  - validate
  - build
  - prepublish
  - publish

validate-packaging:
  image: registry.gitlab.com/centros.one/packaging/cserph:$CSERPH_VERSION
  stage: validate
  script:
    - cserph -n validate

docker-build-master:
  image: docker:$DOCKER_VERSION
  stage: build
  services:
    - docker:$DIND_VERSION
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - apk add jq
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE" --build-arg COMMIT_SHA=${CI_COMMIT_SHA} --build-arg COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG} .
    - docker push "$CI_REGISTRY_IMAGE"
    - printf "IMAGE_DIGEST=%s" $(docker inspect "$CI_REGISTRY_IMAGE" | jq -r  '.[].RepoDigests[0]' | cut -d@ -f2) > $CI_PROJECT_DIR/variables.env
  only:
    - master
  artifacts:
    reports:
      dotenv: variables.env

docker-build:
  image: docker:$DOCKER_VERSION
  stage: build
  services:
    - docker:$DIND_VERSION
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - apk add jq
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" --build-arg COMMIT_SHA=${CI_COMMIT_SHA} --build-arg COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG} .
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
    - printf "IMAGE_DIGEST=%s" $(docker inspect "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" | jq -r  '.[].RepoDigests[0]' | cut -d@ -f2) > $CI_PROJECT_DIR/variables.env
  except:
    - master
  artifacts:
    reports:
      dotenv: variables.env

render-porter-yaml:
  image: registry.gitlab.com/centros.one/packaging/cserph:$CSERPH_VERSION
  stage: prepublish
  script:
    - cserph -n render
      -u /version="$CI_COMMIT_TAG"
      -u /registry="$DOCKER_REPO_PROJECT"
      -u /images/superset/repository="$CI_REGISTRY_IMAGE"
      -u /images/superset/digest="$IMAGE_DIGEST"
  artifacts:
    paths:
      - cnab/porter.yaml
  rules:
    # only if the tag is a valid semver compliant version number
    - if: '$CI_COMMIT_TAG =~ /^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/'

porter-publish:
  image: registry.gitlab.com/centros.one/packaging/porter-docker-runtime:$PORTER_RUNTIME_VERSION
  stage: publish
  services:
    - docker:$DIND_VERSION
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - registry=$(echo $DOCKER_REPO_PROJECT | awk -F/ '{print $1}')
    - docker login -u "$DOCKER_REPO_USER" -p "$DOCKER_REPO_PASSWORD" $registry
  script:
    - cd cnab
    - porter publish
  rules:
    # only if the tag is a valid semver compliant version number
    # (regex taken from https://semver.org/#is-there-a-suggested-regular-expression-regex-to-check-a-semver-string)
    - if: '$CI_COMMIT_TAG =~ /^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/'
